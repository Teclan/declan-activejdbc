package declean.activejdbc.model;

import org.javalite.activejdbc.annotations.Table;

@Table("student")
public class Student extends AbstractModel {

    @Override
    public String getConfigTableName() {
        return "student";
    }

}
