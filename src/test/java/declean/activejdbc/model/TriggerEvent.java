package declean.activejdbc.model;

import org.javalite.activejdbc.annotations.Table;

@Table("EVENTS_TABLE")
public class TriggerEvent extends AbstractModel {

    @Override
    public String getConfigTableName() {
        return "EVENTS_TABLE";
    }

}
