package declean.activejdbc.service;

import declean.activejdbc.model.DbRecord;

public interface RetrieverListener {

    public void recordRetrieved(DbRecord record);

    public DbRecord getDbRecord();

}
